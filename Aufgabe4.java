import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Aufgabe4 {

    public static void main(String[] args) {
        initiate();
    }

    private static void initiate() {
        Integer correctNumbers[] = new Integer[]{3, 7, 12, 18, 37, 42};
        List<Integer> numberList = new ArrayList<>(Arrays.asList(correctNumbers));

        for(int i = 0; i < 50; i++) {
            if(numberList.contains(i)) {
                System.out.println("Die Zahl " + i + " ist in der Ziehung enthalten!");
            } else {
                System.out.println("Die Zahl " + i + " ist leider nicht in der Ziehnung enthalten.");
            }
        }


    }

}
