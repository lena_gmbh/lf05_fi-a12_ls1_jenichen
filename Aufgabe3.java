import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Aufgabe3 {

    public static void main(String[] args) {
        initiate();
    }

    private static void initiate() {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Geben sie 5 Zeichen ein.");
        String chars = scanner.next();

        while (chars.length() > 5) {
            System.out.println("Geben sie 5 Zeichen ein.");
            chars = scanner.next();
        }

        ArrayList<Character> charList = new ArrayList<>();

        for (int i = 0; i < chars.length(); i++) {
            char currChar = chars.charAt(i);
            charList.add(currChar);
        }
        Collections.reverse(charList);
        System.out.println(charList);

    }
}
